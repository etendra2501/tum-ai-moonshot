import argparse

import src
import src.convert as convert


def test_main_function(monkeypatch, capfd):
    def mock_parse_args():
        return argparse.Namespace(input='files/',
                                  output='converted-test-emMRViQz6e',
                                  verbose=True,
                                  workers=1,
                                  multiprocessing=True)

    monkeypatch.setattr(src.convert, 'parse_args', mock_parse_args)

    def mock_run_workers():
        return 0, 0

    monkeypatch.setattr(src.utils, 'run_workers', mock_run_workers)

    convert.main()
    out, err = capfd.readouterr()

    assert 'Conversion has finished successfully!' in out

    # Remove the output directory
    import shutil
    shutil.rmtree('converted-test-emMRViQz6e')
