import json
from pathlib import Path

import pytest

from src.evaluate import evaluate_data


@pytest.fixture
def test_data(tmpdir):
    test_file = Path(tmpdir) / 'test_data.json'
    data = {
        'file1': {'filter': ['filter1', 'filter2', 'filter3']},
        'file2': {'filter': ['filter2', 'filter3', 'filter4']},
    }
    with open(test_file, 'w') as f:
        json.dump(data, f)
    return test_file


@pytest.fixture
def data_file(tmpdir):
    data_file = Path(tmpdir) / 'data_file.json'
    data = {
        'file1': {'filter': ['filter1', 'filter2', 'filter3', 'filter5']},
        'file3': {'filter': ['filter6', 'filter7']},
    }
    with open(data_file, 'w') as f:
        json.dump(data, f)
    return data_file


def test_evaluate_data(test_data, data_file):
    expected = {
        'total': 6,
        'correct': 3,
        'missing': 3,
        'percentage_overlap': 0.5,
    }
    metrics = evaluate_data(test_data, data_file)
    assert metrics == expected
