import pytest
import tempfile
import pathlib
from src.utils import write_data, load_data
from src.dataset_utils import Dataset


@pytest.fixture
def data_dir():
    with tempfile.TemporaryDirectory() as temp_dir:
        data_dir = pathlib.Path(temp_dir)

        for i in range(10):
            file_path = f'data_{i}.gz'
            data = {'id': i, 'data': f'data_{i}'}
            write_data(data, file_path, {'output': data_dir})
        yield data_dir


def test_dataset_iteratively(data_dir):
    dataset = Dataset(data_dir)
    expected_data = [load_data(i) for i in data_dir.glob('*.gz')]
    for i, obj in enumerate(dataset):
        assert obj == expected_data[i]


def test_dataset_in_batches(data_dir):
    dataset = Dataset(data_dir, batch_size=2)
    expected_data = [load_data(i) for i in data_dir.glob('*.gz')]
    batch_num = 0
    for batch in dataset.get_batches():
        assert len(batch) == 2
        expected_batch = expected_data[batch_num:batch_num + 2]
        assert batch == expected_batch
        batch_num += 2


def test_dataset_len(data_dir):
    dataset = Dataset(data_dir)
    assert len(dataset) == 10
