import gzip
import os
import pathlib
import tempfile
from multiprocessing import Pool, cpu_count

import orjson
import pytest
import tqdm
from pydantic import BaseModel, ValidationError

from src.utils import load_data, write_data, run_workers, \
    validate_json, convert_data


@pytest.mark.parametrize('filename, json_obj', [
    (tempfile.NamedTemporaryFile(mode='w', delete=False,
                                 suffix='.gz').name,
     '{"foo":"bar","baz":[1,2,3]}'),
])
def test_write_data(filename, json_obj):
    filename = pathlib.Path(filename)

    kwargs = {'output': filename.parents[0]}

    # Write JSON object to file
    write_data(json_obj, filename, kwargs)

    # Read file contents and check if
    # they match the expected output
    output = load_data(filename)

    assert output == json_obj

    # Clean up temporary files
    os.unlink(filename)


@pytest.mark.parametrize('filename, expected_output', [
    (tempfile.NamedTemporaryFile(mode='w', delete=False,
                                 suffix='.gz').name, {}),
    (tempfile.NamedTemporaryFile(mode='w', delete=False,
                                 suffix='.gz').name, {'foo': 'bar'}),
    (tempfile.NamedTemporaryFile(mode='w', delete=False,
                                 suffix='.json').name, {'baz': 'qux'}),
    ('nonexistentfile.json', None),
])
def test_load_data(filename, expected_output):
    filename = pathlib.Path(filename)

    # Gzip file if necessary
    if filename.suffix == '.gz':
        with gzip.open(filename, 'w') as f:
            f.write(orjson.dumps(expected_output))
    else:
        with open(filename, 'w') as f:
            f.write(orjson.dumps(expected_output).decode('utf-8'))

    # Test load_data function
    assert load_data(filename) == expected_output

    # Clean up temporary files
    os.unlink(filename)


def run_fn(worker_arg):
    filename = worker_arg[0]
    success = worker_arg[1]
    return filename, success


@pytest.fixture
def workers_pool():
    return Pool(cpu_count())


@pytest.fixture
def worker_args():
    return [
        ('file1', 1),
        ('file2', 1),
        ('file3', 0),
        ('file4', 1),
        ('file5kmvkldvlkjdsvdsvdsvsdvsdvsdvsdvsdvsdvsdvsdvdsvsd'
         'vsdvsdvdsnvkjsndkvjnsdkmkldsmvlkdslkvndslkvnlksd', 0),
    ]


@pytest.fixture
def progress_bar(worker_args):
    return tqdm.tqdm(total=len(worker_args))


@pytest.mark.parametrize('num_doms, num_skipped, verbose', [
    (3, 2, False),
    (3, 2, True),
])
def test_run_workers(workers_pool, worker_args, progress_bar,
                     num_doms, num_skipped, verbose):
    actual_num_doms, actual_num_skipped = run_workers(workers_pool,
                                                      worker_args,
                                                      run_fn,
                                                      progress_bar,
                                                      verbose=verbose)
    assert actual_num_doms == num_doms
    assert actual_num_skipped == num_skipped


class Data(BaseModel):
    name: str
    age: int
    email: str


@pytest.fixture
def schema():
    return Data


@pytest.mark.parametrize('json_obj, expected_data', [
    ({'name': 'John Doe', 'age': 30,
      'email': 'john.doe@example.com'},
     {'name': 'John Doe', 'age': 30,
      'email': 'john.doe@example.com'}),
    ({'name': 'Jane Doe', 'age': 'invalid',
      'email': 'jane.doe@example.com'},
     pytest.raises(ValidationError)),
])
def test_validate_json(json_obj, schema, expected_data):
    if isinstance(expected_data, dict):
        actual_data = validate_json(json_obj, schema)
        assert actual_data == expected_data
    else:
        with expected_data:
            validate_json(json_obj, schema)


@pytest.fixture
def kwargs1():
    req_files = [tempfile.NamedTemporaryFile(mode='w', delete=False,
                                             suffix='.gz').name,
                 tempfile.NamedTemporaryFile(mode='w', delete=False,
                                             suffix='.gz').name]
    dom_recs = [tempfile.NamedTemporaryFile(mode='w', delete=False,
                                            suffix='.json').name,
                tempfile.NamedTemporaryFile(mode='w', delete=False,
                                            suffix='.json').name]
    return {
        'file': pathlib.Path(
            tempfile.NamedTemporaryFile(mode='w',
                                        delete=False,
                                        suffix='.gz').name),
        'drec_path': pathlib.Path(dom_recs[0]).parents[0],
        'dom_recs': dom_recs,
        'req_files': req_files,
        'req_path': pathlib.Path(req_files[0]).parents[0],
    }


def test_convert_data_no_match(kwargs1):
    expected = [kwargs1['file'].name, 0]
    actual = convert_data(kwargs1)

    assert actual == expected

    # Clean up temporary files
    os.unlink(kwargs1['file'])
    os.unlink(kwargs1['dom_recs'][0])
    os.unlink(kwargs1['dom_recs'][1])
    os.unlink(kwargs1['req_files'][0])
    os.unlink(kwargs1['req_files'][1])


@pytest.fixture
def kwargs2():
    cur_dir = os.sep.join(__file__.split(os.sep)[:-1])
    return {
        'file': pathlib.Path(
            os.sep.join([cur_dir,
                         'files/doms/filename1--url.json.gz'])),
        'drec_path': pathlib.Path(
            os.sep.join([cur_dir, 'files/domain-records/'])),
        'dom_recs': [pathlib.Path(
            os.sep.join([cur_dir,
                         'files/domain-records/filename1.json']))],
        'req_files': [pathlib.Path(
            os.sep.join([cur_dir,
                         'files/requests/filename1--url.json.gz']))],
        'req_path': pathlib.Path(
            os.sep.join([cur_dir, 'files/requests/'])),
        'output': tempfile.TemporaryDirectory(),
    }


def test_convert_data_with_match(kwargs2):
    expected = [kwargs2['file'].name, 0]
    actual = convert_data(kwargs2)
    assert actual == expected
