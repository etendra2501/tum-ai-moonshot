# TUM ai Moonshot :rocket:

## Description
This repo contains scripts to convert and process the crawled dataset as well as scripts to load and write data objects, get training dataset and evaluate results against a dataset.

## Installation
Clone this repo to get started:
```bash
git clone git@gitlab.com:eyeo/machine-learning/tum-ai-moonshot.git
cd tum-ai-moonshot
```
Install the required packages in `requirements.txt` to use the scripts.
We recommend creating a virtualenv and installing the required dependencies there:
```bash
python -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```
To leave the virtual environment, use: `deactivate`  
To reactivate the environment: `. venv/bin/activate`

## Usage
There are various scripts in the project.

### 1. Data loader

The dataloader contains a class that can be used to load the data and use it either with
batches or iteratively. This is useful since the dataset may be too large to be loaded in memory at once.
The loader makes use of python generators to load only batches of data at once into the memory.

To create a dataset object with a given data directory and batch size:
```python
from src.dataset_utils import Dataset
dataset = Dataset(data_dir, batch_size=2)
```
Now with this dataset you can either iteratively access individual data points by running it in a loop or enumerating
```python
for obj in dataset:
    print(obj)
```
And for batched loading
```python
for batch in dataset.get_batches():
    #batch will have a list of objects according to batch size
    print(batch)
```
See an example in `src/sample_loader.py`. To see the available options, run
```bash
python src/sample_loader.py --help
```

### 2. Evaluation

This script provides a function for evaluating data based on exact matches between filter rules in a test set and a data file.

This function takes two parameters, `benchmark_set` and `data_file`, both of which are `pathlib.Path` objects. The `benchmark_set` parameter should point to a JSON file containing benchmark data in the following format:

```json
{
"filename": {
  "filter": ["filter_rule_1", "filter_rule_2", ...],
  "id": "unique_id"
},
...
}
```
Sample of the evaluation [data file is given here](https://gitlab.com/eyeo/machine-learning/tum-ai-moonshot/-/blob/main/sample-files/evaluation_file.json).

The `data_file` parameter should point to a JSON file containing data to be evaluated in the same format as the benchmark dataset.

The function returns a dictionary containing the following metrics:
- `total`: the total number of filter rules in the benchmark dataset
- `correct`: the number of filter rules in the benchmark dataset that match exactly with filter rules in the data file
- `missing`: the number of filter rules in the benchmark dataset that do not appear in the data file
- `filter_overlap`: the proportion of filter rules in the benchmark dataset that match exactly with filter rules in the data file

#### Example Usage
```python
from pathlib import Path
from src.evaluate import evaluate_data

benchmark_dataset = Path('benchmark_data.json.gz')
data_file = Path('data_to_evaluate.json.gz')

metrics = evaluate_data(benchmark, data_file)

print(metrics)
```
See an example in `src/sample_loader.py`. To see the available options, run
```bash
python src/sample_evaluator.py --help
```
See an example run comparing the sample file to itself:
```bash
python src/sample_evaluator.py sample-files/evaluation_file.json sample-files/evaluation_file.json
```

### Notes
* Filter rules in the test set are matched exactly with filter rules in the data file, so there is no scope for fuzzy matching or partial matches.
* The id field in the test set and data file is not used for evaluation, but is used for tracking and organization.
