# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from pathlib import Path

from tqdm import tqdm

from utils import load_data


def evaluate_data(test_set: Path, data_file: Path) -> dict:
    """
    Evaluate the percentage overlap of filter matches.

    :param test_set: Path object pointing to the
    file containing the test set data.
    :param data_file: Path object pointing to the
    file containing the data to be evaluated.

    :return: Dictionary containing evaluation metrics,
    including total test points, exact matches (by filter),
        missing/wrong filters, and percentage filter overlap.
    """
    # Read in the test set
    test_data = load_data(test_set)

    # Read in the data file
    data = load_data(data_file)

    # Initialize evaluation counts
    total = 0  # total number of test points
    correct = 0  # number of test points with exact matches (by filter)
    missing = 0  # number of missing or wrong filters in the predicted data

    # Loop over the test set
    for filename, test_filters in tqdm(test_data.items()):
        # Check if the filename is in the predicted data
        if filename in data:
            # Check the number of matching filters
            matched_filters = set(test_filters['filter'])\
                              & set(data[filename]['filter'])
            num_matched_filters = len(matched_filters)
            correct += num_matched_filters
            # Calculate number of missing or wrong
            # filters in the predicted data
            missing += (len(test_filters['filter']) -
                        num_matched_filters)
        else:
            # All filters in the test data are missing in the predicted data
            missing += len(test_filters['filter'])
        total += len(test_filters['filter'])

    # Calculate the evaluation metric
    percentage_overlap = float(correct) / total if total > 0 else 0.0

    # Store the evaluation metrics in a dictionary
    metrics = {
        'total': total,
        'correct': correct,
        'missing': missing,
        'percentage_overlap': percentage_overlap,
    }

    # Print the evaluation report
    print('Evaluation Report:')
    print('-------------------')
    print('Total test points: {}'.format(total))
    print('Exact matches (by filter): {}'.format(correct))
    print('Missing/wrong filters: {}'.format(missing))
    print('Percentage Overlap: {:.4f}'.format(percentage_overlap))

    # Return the evaluation metrics dictionary
    return metrics
