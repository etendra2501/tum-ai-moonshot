# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import gzip
import logging
import uuid

import orjson

from schema import Request, Dom


def run_workers(workers_pool, worker_args, run_fn,
                progress_bar, verbose=False):
    num_doms = 0
    num_skipped = 0
    try:
        for results in workers_pool.imap_unordered(run_fn, worker_args):
            # use this for single threaded debug :
            # for results in map(run_fn, worker_args):

            filename, success = results
            if success == 1:
                num_doms += 1
            else:
                num_skipped += 1
            if verbose:
                slen = len(filename)
                if slen < 100:
                    pstr = ' ' * (100 - slen) + filename
                else:
                    pstr = filename[:100]
                progress_bar.set_postfix_str(pstr)

            progress_bar.update()
    except KeyboardInterrupt:
        print('Stopped by keyboard interrupt')
        workers_pool.terminate()
    finally:
        workers_pool.close()

    return num_doms, num_skipped


def validate_json(json_obj, schema):
    data = schema.parse_obj(json_obj)
    return data.dict()


def load_data(filename):
    # This parsing helps ensure the integrity of the data. If any data
    # extraction step fails, don't use that data. See SP-1958.
    # gzip -> dict

    if filename.suffix == '.gz':
        open_fn = gzip.open
    else:
        open_fn = open

    with open_fn(filename, mode='r') as in_stream:
        try:
            objs = orjson.loads(in_stream.read())
            return objs
        except BaseException:
            logging.warning('Could not load json from {}'
                            .format(filename.name))


def write_data(json_obj, filename, kwargs):
    # Write a JSON to a gzipped compressed file

    with gzip.open(kwargs['output'].joinpath(filename), 'w') as fl:
        fl.write(orjson.dumps(json_obj))


def convert_data(kwargs):
    status = 0
    filename = kwargs['file']
    obj = load_data(filename)

    dom_rec_fname = filename.name.split('--url')[0] + '.json'
    dom_rec_fname = kwargs['drec_path'].joinpath(dom_rec_fname)

    req_fname = kwargs['req_path'].joinpath(filename.name)

    if dom_rec_fname in kwargs['dom_recs'] and \
            req_fname in kwargs['req_files']:
        dom_rec = load_data(dom_rec_fname)
        reqs = load_data(req_fname)
    else:
        return [filename.name, 0]

    try:
        # conversion
        obj['domain'] = dom_rec['domain']
        obj['completed'] = dom_rec['dateCompleted']
        reqs_fhits, reqs_no_fhits = [], []

        for req in reqs:
            filter_hash = req['filter'].get('filterHash', None)
            if filter_hash is not None:
                filter_value = dom_rec['filters'].get(filter_hash, '')
                req['filter']['filterValue'] = filter_value
                req = validate_json(req, Request)
                reqs_fhits.append(req)
            else:
                req = validate_json(req, Request)
                reqs_no_fhits.append(req)
        reqs = {
            'withFilterHits': reqs_fhits,
            'noFilterHits': reqs_no_fhits,
        }
        obj['requests'] = reqs
        obj['filters'] = list(dom_rec['filters'].values())
        obj['id'] = str(uuid.uuid4())

        obj = validate_json(obj, Dom)

        write_data(obj, filename.name, kwargs)

        status = 1
    except ValueError as err:
        print('Skipping: {}'.format(kwargs['file']))
        print('ValueError: {}'.format(err))
    except AttributeError as err:
        print('Skipping: {}'.format(kwargs['file']))
        print('AttributeError: {}'.format(err))
    except TypeError as err:
        print('Skipping: {}'.format(kwargs['file']))
        print('TypeError: {}'.format(err))

    return [filename.name, status]
