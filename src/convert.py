# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Convert graphs from JSON to trainable formats."""

import argparse
import logging
import multiprocessing
import multiprocessing.pool as mp_pool
import pathlib
import sys
import time

import tqdm

from utils import convert_data, run_workers

FINAL_REPORT_TEMPLATE = '''
Conversion has finished successfully! \x1b[m
========== REPORT =========
# of doms processed : {num_doms}
# of doms skipped : {num_skipped}


Time: {seconds} seconds == {minutes} minutes == {hours} hours

'''


def parse_args():
    parser = argparse.ArgumentParser(
        'Convert and combine JSON data from crawl to single JSON',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('input',
                        metavar='INPUT_DIR',
                        help='Input directory with gzipped '
                             'JSON graphs and directories')
    parser.add_argument('output',
                        metavar='OUTPUT_DIR',
                        help='Output directory converted data')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Report info')
    parser.add_argument('-w',
                        '--workers',
                        type=int,
                        default=1,
                        help='Number of workers')
    parser.add_argument('-m',
                        '--multiprocessing',
                        action='store_false',
                        default=True,
                        help='Flag to use multiprocessing or not, turned on '
                             'by default')

    return parser.parse_args()


def main():
    args = parse_args()

    start = time.time()

    if args.verbose:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    output_dir = pathlib.Path(args.output)
    input_dir = pathlib.Path(args.input)

    output_dir.mkdir(exist_ok=True, parents=True)
    dom_files = input_dir.glob('doms/*.gz')
    dom_files = list(dom_files)

    req_path = input_dir.joinpath('requests')
    drec_path = input_dir.joinpath('domain-records')

    req_files = req_path.glob('*.gz')
    req_files = list(req_files)

    dom_recs = drec_path.glob('*.json')
    dom_recs = list(dom_recs)

    # Setting up multiprocessing
    workers_pool = mp_pool.ThreadPool(args.workers)
    if args.multiprocessing:
        workers_pool = multiprocessing.Pool(args.workers)

    # Setting worker arguments
    kwargs = vars(args)
    kwargs['output'] = output_dir
    kwargs['req_files'] = req_files
    kwargs['dom_recs'] = dom_recs
    kwargs['req_path'] = req_path
    kwargs['drec_path'] = drec_path

    worker_args = [
        dict({
            'file': f,
        }, **kwargs) for f in dom_files
    ]

    progress_bar = tqdm.tqdm(dom_files,
                             desc='Converting files',
                             total=len(dom_files))

    # Converting data
    res = run_workers(workers_pool,
                      worker_args,
                      convert_data,
                      progress_bar,
                      verbose=args.verbose)

    # Outputting stats
    num_doms, num_skipped = res

    end = time.time()
    duration = end - start

    print(
        FINAL_REPORT_TEMPLATE.format(
            num_doms=num_doms,
            num_skipped=num_skipped,
            seconds=round(duration, 2),
            minutes=round(duration / 60, 2),
            hours=round(duration / 3600, 2),
        ))


if __name__ == '__main__':
    main()
