# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import argparse
from pathlib import Path

from dataset_utils import Dataset

parser = argparse.ArgumentParser(
    'Convert and combine JSON data from crawl to single JSON',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('input',
                    metavar='INPUT_DIR',
                    help='Path to input directory')
parser.add_argument('output',
                    metavar='OUTPUT_DIR',
                    help='Path to output directory')
parser.add_argument('-b', '--batch_size', type=int, default=2)
args = parser.parse_args()

# Load dataset
data_dir = Path(args.input)
dataset = Dataset(data_dir, batch_size=args.batch_size)

# Load dataset objects iteratively
for obj in dataset:
    # TODO: Do something with each obj here
    pass

# Load dataset in batches
for batch in dataset.get_batches():
    # TODO: AND/OR do something with each batch here
    pass
