# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from pydantic import BaseModel
from typing import Optional


# Define a Pydantic model that corresponds to the schema
class RequestData(BaseModel):
    frameId: int
    frameType: Optional[str]
    fromCache: Optional[bool]
    initiator: Optional[str]
    method: Optional[str]
    parentFrameId: Optional[int]
    requestId: Optional[str]
    tabId: Optional[int]
    timeStamp: Optional[float]
    type: Optional[str]
    url: str


class FilterData(BaseModel):
    filterValue: Optional[str]
    type: Optional[str]


class Request(BaseModel):
    request: RequestData
    filter: Optional[FilterData]


class Dom(BaseModel):
    id: str
    url: str
    dom: dict
    childFrames: list
    domain: str
    completed: str
    requests: dict
    filters: list
