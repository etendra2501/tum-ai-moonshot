# Copyright (C) 2023-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from utils import load_data


class Dataset:
    """

    Dataset class.

    Use this class to load dataset and batch
    the data for saving memory.

    """

    def __init__(
            self, dataset_path, batch_size: int = 1):
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.file_paths = self.load_file_paths()
        self.size = len(self.file_paths)

    def load_file_paths(self):
        return list(self.dataset_path.glob('*.gz'))

    def load_data_iteratively(self):
        for file_path in self.file_paths:
            obj = load_data(file_path)
            yield obj

    def load_data_in_batches(self):
        if self.batch_size is None:
            raise ValueError('Batch size must be '
                             'specified to load data in batches.')
        data = []
        for file_path in self.file_paths:
            obj = load_data(file_path)
            data.append(obj)
            if len(data) == self.batch_size:
                yield data
                data = []

        if data:
            yield data

    def __iter__(self):
        yield from self.load_data_iteratively()

    def get_batches(self):
        yield from self.load_data_in_batches()

    def __len__(self):
        return self.size
